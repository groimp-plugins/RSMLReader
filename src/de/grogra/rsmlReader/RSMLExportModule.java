package de.grogra.rsmlReader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.vecmath.Point3d;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.edit.GraphSelection;
import de.grogra.vecmath.Matrix34d;
import de.grogra.xl.impl.base.EdgeIterator;
import de.grogra.xl.query.EdgeDirection;

public class RSMLExportModule extends FilterBase implements FileWriterSource {
	static de.grogra.rgg.model.Runtime run = new de.grogra.rgg.model.Runtime();
	int id=0;	
		public RSMLExportModule(FilterItem item, FilterSource source) {
			super(item, source);
			setFlavor (item.getOutputFlavor ());


		}

		public void write(File out) throws IOException {
			
			// find root node of scene
			Node rootNode = UI.getRootOfProjectGraph(Workbench.current());
			// then find the rggRoot
			String erg="fail";
			//Node rggRoot = rootNode.getFirstEdge().getTarget();
			Workbench w = Workbench.current ();
			Object s = UIProperty.WORKBENCH_SELECTION.getValue (w);
			if(s instanceof GraphSelection){
				rootNode = (Node) ((GraphSelection)s).getObject(0);
			}
			
			try {
				erg=crawl(rootNode).toString();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			FileWriter fw = new FileWriter(out);
			
			erg="<?xml version='1.0' encoding='UTF-8'?>\n"
					+ "<rsml xmlns:po=\"http://www.plantontology.org/xml-dtd/po.dtd\">\n"
					+ "	<metadata>\n"
					+ "		<software>GroIMP</software>\n"
					+ "	</metadata>\n"
					+ "	<scene>\n"
					+ "		<plant id=\"1\">"+erg+"</plant></scene></rsml>";
			fw.write(erg);
			fw.close();
		}

		public RootPart crawl(Node n) throws ClassNotFoundException {
			RootPart rp_tmp=null;
			for (EdgeIterator i = run.createEdgeIterator(n, EdgeDirection.FORWARD); i.hasEdge(); i.moveToNext()) {
				Edge e2 = n.getTarget().getEdgeTo((Node) i.target);
				if(e2.getEdgeBits()==Graph.SUCCESSOR_EDGE) {
					rp_tmp=crawl((Node)i.target);
				}
			}
			if(rp_tmp==null) {
				rp_tmp=new RootPart();
			}
			Class c = Class.forName("de.grogra.turtle.F");
			if(c.isInstance(n)) {
				Matrix34d md = GlobalTransformation.get (n, false, GraphState.current (n.getGraph ()), true);
				if(rp_tmp.geometry.length()==0) {
					Point3d p = de.grogra.rgg.Library.endlocation(n);
					rp_tmp.addGeo("<point x=\""+p.x+"\" y=\""+p.y+"\" z=\""+p.z+"\"/>\n");					
					rp_tmp.addFunc("<sample value=\""+((de.grogra.turtle.F)n).diameter+"\"/>\n") ;
				}
				Point3d p = de.grogra.rgg.Library.location(n);
				rp_tmp.addGeo("<point x=\""+p.x+"\" y=\""+p.y+"\" z=\""+p.z+"\"/>\n");
				rp_tmp.addFunc("<sample value=\""+((de.grogra.turtle.F)n).diameter+"\"/>\n") ;
			}
			for (EdgeIterator i = run.createEdgeIterator(n, EdgeDirection.FORWARD); i.hasEdge(); i.moveToNext()) {
				Edge e2 = n.getTarget().getEdgeTo((Node) i.target);
				if(e2.getEdgeBits()==Graph.BRANCH_EDGE) {
					rp_tmp.addSubRoot(crawl((Node)i.target));
				}
			}
			return rp_tmp;
		}
	}
/*		public String crawl(Edge ed) throws ClassNotFoundException {
			String erg="";
			Class c = Class.forName("de.grogra.turtle.F");
			if(c.isInstance(ed.getTarget())) {
			 //	Point3d p = location(ed.getTarget());
				//((de.grogra.turtle.F)ed.getTarget()).getTranslation();
				Node node=ed.getTarget();
				Matrix34d md = GlobalTransformation.get (node, false, GraphState.current (node.getGraph ()), false);
				
				erg+="F "+ md.m03  +","+ md.m13  +","+ md.m23  +"\n";
			}
			for (EdgeIterator i = run.createEdgeIterator(ed.getTarget(), EdgeDirection.FORWARD); i.hasEdge(); i.moveToNext()) {
				Edge e2 = ed.getTarget().getEdgeTo((Node) i.target);
				if(e2.getEdgeBits()==Graph.SUCCESSOR_EDGE) {
					erg+=crawl(e2);
				}
			}
			for (EdgeIterator i = run.createEdgeIterator(ed.getTarget(), EdgeDirection.FORWARD); i.hasEdge(); i.moveToNext()) {
				Edge e2 = ed.getTarget().getEdgeTo((Node) i.target);
				if(e2.getEdgeBits()== Graph.BRANCH_EDGE) {
					String tmp=crawl(e2);
					if(tmp.startsWith("<root")) {
						erg+=tmp;
					}else {
						erg+="<root>\n"+tmp+"</root>\n";
					}
				}
			}
			
			return erg;
		}

	}
*/
class RootPart{
	static int id=0;
	String geometry;
	String function;
	ArrayList<RootPart> subroots;
	public RootPart() {
		geometry="";
		function="";
		subroots= new ArrayList<RootPart>();

	}

	public RootPart(String geo, String func) {
		geometry =geo;
		function = func;
		subroots= new ArrayList<RootPart>();
	}
	public void addGeo(String g) {
		geometry=g+geometry; //added at the beginning because of backwards crowling 
	}
	public void addFunc(String f) {
		function = f+function; //added at the beginning because of backwards crowling 
	}
	public void addSubRoot(RootPart rp) {
		subroots.add(rp);
	}
	
	public String toString() {
		String erg="";
		for(RootPart rp: subroots) {
			erg+=rp.toString();
		}
		if(geometry.length()>0) {
		erg="<root id=\""+id+"\">\n"+"<geometry>\n<polyline>\n"+geometry+"</polyline>\n</geometry>\n<functions>\n<function domain=\"polyline\" name=\"diameter\">\n"+function+"</function></functions>\n"+erg+"</root>";
		id++;
		}
		return erg;
	}
}

